import case
def main():
    choix=0
    while choix!=4:
        print("------------------------------------------")
        print("Bienvenue dans le menu de jeu")
        print("""Veuilles choisir entre:
          1)Jeu de dé
          2)Jeu de nombre
          3)Jeu du pendu
          4)Quitter le jeu """)

        print("------------------------------------------")
        choix = int(input("Choisissez un numéro pour lancer votre jeu : "))
        print("------------------------------------------")
        case.menu(choix)
        if choix==1:
            case.case1()
            continue
        elif choix==2:
            print("Vous avez choisi le jeu de la devinette")
            i=0
            while i<3:
                valeur=int(input("Entrer votre chiffre: "))
                case.case2(valeur)
                i=i+1
            if i==3: print("C'etait votre dernière chance") 
            continue
        elif choix==3:
            print("Vous avez choisi le jeu du pendu")
            case.case3()
            continue
        elif choix==4:
            break
    
        else:
            print("Appuyer sur une touche pour revenir au menu principal")


if __name__=="__main__":
    main()

